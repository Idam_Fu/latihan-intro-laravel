<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function Register(){
        return view('register');
    }

    public function postRegister(Request $request){
        $firstName = $request->input("FName");
        $lastName = $request->input("LName");

        // dd($firstName);

        return view('welcome',['Fname' => $firstName,'Lname' => $lastName]);
    }
    

}
