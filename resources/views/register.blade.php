<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <!-- Nama : Muh. Ilyas Istiqama. M
    Email : ilyasistiqama@gmail.com -->
    
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
    @csrf
        <label for="fn">First name:</label> <br><br>
            <input type="text" id="fn" name="FName" required> <br><br>
        
        <label for="ln">Last name:</label> <br><br>
            <input type="text" id="ln" name="LName" required> <br><br>

        <label for="gr">Gender:</label> <br><br>
            <input type="radio" name="gender" value="Male" checked>Male <br>
            <input type="radio" name="gender" value="Female">Female <br>
            <input type="radio" name="gender" value="Other">Other <br><br>

        <label>Nationality:</label> <br><br>
            <select>
                <option value="id">Indonesian</option>
                <option value="sg">Singaporeans</option>
                <option value="sg">Malaysian</option>
                <option value="aus">Australian</option>
            </select> <br><br>

        <label for="bhs">Language Spoken:</label> <br>
            <input type="checkbox" name="bahasa" value="idn" id="bhs" checked>Bahasa Indonesia <br>
            <input type="checkbox" name="bahasa" value="eng" id="bhs">English <br>
            <input type="checkbox" name="bahasa" value="other" id="bhs">Other <br><br>
        
        <label for="bio">Bio:</label> <br><br>
            <textarea name="bio" id="bio" cols="30" rows="10">Saya adalah ...</textarea> <br>

        <input type="submit" value="Sign Up">

    </form>
</body>
</html>